/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Joshua
 */
public class Agenda {

    static ControladorAgenda agenda = new ControladorAgenda();
    static ArrayList<Contacto> contactos = new ArrayList<>();

    public static Scanner leer = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        agenda.cargar();
        Menu();
    }

    public static void Menu() {
        int opcion;
        do {
            System.out.println("=========Menu=========");
            System.out.println("1-Insertar contactos");
            System.out.println("2-Eliminar contactos");
            System.out.println("3-Buscar contactos");
            System.out.println("4-Mostrar contactos Actuales");
            System.out.println("5-Actualizar contactos");
            System.out.println("6-Guardar");
            System.out.println("7-Salir");
            opcion = leer.nextInt();
            switch (opcion) {
                case 1:
                    agenda.agregar();
                    agenda.mostrar();
                    break;
                case 2:
                    int opcion1;
                    do {
                        agenda.mostrar();
                        agenda.eliminar();
                        agenda.mostrar();
                        do {
                            System.out.println("Deseas borrar otro contacto?");
                            System.out.println("1-Si");
                            System.out.println("2-No");
                            opcion1=leer.nextInt();
                            if(opcion1<1 || opcion1>2){
                                System.out.println("Opcion no valida");
                            }
                        } while(opcion1<1 || opcion1>2);
                    }while(opcion1==1); 
                    break;
                case 3:
                    agenda.buscar();
                    agenda.mostrar();
                    break;
                case 4:
                    agenda.mostrar();
                    break;
                case 5:
                    agenda.mostrar();
                    agenda.actualizar();
                    agenda.mostrar();
                    break;
                case 6:
                    if (agenda.guardar()) {
                        System.out.println("El archivo se ha guardado correctamente");
                    } else {
                        System.out.println("El archivo no se ha podio guardar");
                    }
                    agenda.mostrar();
                    break;
                case 7:
                    System.out.println("Que tenga un buen dia");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Opcion no valida");
            }

        } while (opcion
                != 7);
    }

    /*
    private void agregar() {
        int opcion;
        do {

            String nombre, numTel, numCasa, email;
            leer.nextLine();
            System.out.println("Intruduzca el nombre");
            nombre = leer.nextLine();
            System.out.println("Intruduzca el numero telefonico");
            numTel = leer.nextLine();
            System.out.println("Intruduzca el numero telefonico de casa");
            numCasa = leer.nextLine();
            System.out.println("el email");
            email = leer.nextLine();
            contactos.add(new Contacto(nombre, numTel, email, numCasa));
            agenda.mostrar();
            do {
                System.out.println("Desea insertar otro contacto?");
                System.out.println("1-Si");
                System.out.println("2-No");
                opcion = leer.nextInt();
                switch (opcion) {
                    case 1:
                        break;
                    case 2:
                        break;
                    default:
                        System.out.println("Opcion no valida");
                }
            } while (opcion < 1 || opcion > 2);
        } while (opcion == 1);
    }

    private void eliminar() {
        System.out.println("Ingrese el numero del contacto que desea borrar");
        int opc = leer.nextInt();
        if (opc != -1) {
            opc = opc - 1;
            contactos.remove(opc);
            System.out.println("Contacto borrado");
        } else {
            System.out.println("No se encontro el dato");
        }
    }

    private void buscar() {
        int posicion, cont = 0, opcion;
        do {
            System.out.println("Introduzca el numero del contacto que desea buscar");
            posicion = leer.nextInt();
            for (Contacto contacto : contactos) {
                cont++;
                if (cont == posicion) {
                    System.out.println(cont + " " + contacto);
                }
            }
            do {
                System.out.println("Desa buascar otro contacto");
                System.out.println("1-Si");
                System.out.println("2-No");
                opcion = leer.nextInt();
            } while (opcion < 1 || opcion > 2);
        } while (opcion == 1);
    }

    private static void mostrar() {
        int cont = 0;
        for(Contacto contacto : contactos){
            cont++;
            System.out.println(cont+" "+contacto);
        }
    }

    private void actualizar() {
        int opcion;
        String nombre, numTel, numCasa, email;
        int registro, posicion, cont = 0;
        do {
            System.out.println("Introduzca el numero del registro a modificar");
            posicion = leer.nextInt();
            do {
                System.out.println("Tecla 1 en el caso de que desee modificar el nombre");
                System.out.println("Teclea 2 en el caso de que desee modificar el numero de telefono");
                System.out.println("Teclea 3 en el caso de que desee modificar el  E-mail");
                System.out.println("Teclea 4 en el caso de que desee modificar el numero de casa");
                System.out.println("Teclea el numero del dato que desees modificar: ");
                opcion = leer.nextInt();
                switch (opcion) {
                    case 1:
                        System.out.println("Intruduzca el nombre");
                        nombre = leer.nextLine();
                        nombre = leer.nextLine();
                        for (Contacto contacto : contactos) {
                            cont++;
                            if (cont == posicion) {
                                numTel = contacto.getNumTel();
                                email = contacto.getEmail();
                                numCasa = contacto.getNumCasa();
                                contacto.setNombre(nombre);
                                contacto.setNumTel(numTel);
                                contacto.setEmail(email);
                                contacto.setNumCasa(numCasa);
                            }
                        }
                        break;
                    case 2:
                        System.out.println("Intruduzca el telefono");
                        numTel = leer.nextLine();
                        numTel = leer.nextLine();
                        for (Contacto contacto : contactos) {
                            cont++;
                            if (cont == posicion) {
                                nombre = contacto.getNombre();
                                email = contacto.getEmail();
                                numCasa = contacto.getNumCasa();
                                contacto.setNombre(nombre);
                                contacto.setNumTel(numTel);
                                contacto.setEmail(email);
                                contacto.setNumCasa(numCasa);
                            }
                        }
                        break;
                    case 3:
                        System.out.println("Intruduzca el email");
                        email = leer.nextLine();
                        email = leer.nextLine();
                        for (Contacto contacto : contactos) {
                            cont++;
                            if (cont == posicion) {
                                nombre = contacto.getNombre();
                                numTel = contacto.getNumTel();
                                numCasa = contacto.getNumCasa();
                                contacto.setNombre(nombre);
                                contacto.setNumTel(numTel);
                                contacto.setEmail(email);
                                contacto.setNumCasa(numCasa);
                            }
                        }
                        break;
                    case 4:
                        System.out.println("Intruduzca el numero de la casa");
                        numCasa = leer.nextLine();
                        numCasa = leer.nextLine();
                        for (Contacto contacto : contactos) {
                            cont++;
                            if (cont == posicion) {
                                nombre = contacto.getNombre();
                                numTel = contacto.getNumTel();
                                email = contacto.getEmail();
                                contacto.setNombre(nombre);
                                contacto.setNumTel(numTel);
                                contacto.setEmail(email);
                                contacto.setNumCasa(numCasa);
                            }
                        }
                        break;
                    default:
                        System.out.println("Opcion no valida");
                }
            } while (opcion < 1 || opcion > 4);
            do {
                System.out.println("Desa buascar otro modificar");
                System.out.println("1-Si");
                System.out.println("2-No");
                opcion = leer.nextInt();
            } while (opcion < 1 || opcion > 2);
            cont=0;
        } while (opcion == 1);
    }
     */
    public boolean guardar() {

        ControladorAgenda obj = new ControladorAgenda();
        return obj.guardar((Object) contactos);

    }

}
