/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import static agenda.Agenda.contactos;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Joshua
 */
public class ControladorAgenda {
   private Scanner leer = new Scanner(System.in);
   //ControladorAgenda agenda = null;
   
   ControladorAgenda(){
       
   }
   
    public void agregar() {
        int opcion;
        do {

            String nombre, numTel, numCasa, email;
            leer.nextLine();
            System.out.println("Intruduzca el nombre");
            nombre = leer.nextLine();
            System.out.println("Intruduzca el numero telefonico");
            numTel = leer.nextLine();
            System.out.println("Intruduzca el numero telefonico de casa");
            numCasa = leer.nextLine();
            System.out.println("el email");
            email = leer.nextLine();
            contactos.add(new Contacto(nombre, numTel, email, numCasa));
            mostrar();
            do {
                System.out.println("Desea insertar otro contacto?");
                System.out.println("1-Si");
                System.out.println("2-No");
                opcion = leer.nextInt();
                switch (opcion) {
                    case 1:
                        break;
                    case 2:
                        break;
                    default:
                        System.out.println("Opcion no valida");
                }
            } while (opcion < 1 || opcion > 2);
        } while (opcion == 1);
    }

    public void eliminar() {
        System.out.println("Ingrese el numero del contacto que desea eliminar");
        int opc = leer.nextInt();
        if (opc != -1) {
            System.out.println("Desea eliminar el contacto, 1= Si 2= No");
            int resp = leer.nextInt();
            if(resp==1){
                opc = opc - 1;
                contactos.remove(opc);
                System.out.println("Contacto elegido eliminado");
            } else {
                System.out.println("Contacto elegido no eliminado");
            }
        } else {
            System.out.println("No se encontro el contacto");
        }
    }

    public void buscar() {
        int posicion, cont = 0, opcion;
        do {
            System.out.println("Introduzca el numero del contacto que desea buscar");
            posicion = leer.nextInt();
            for (Contacto contacto : contactos) {
                cont++;
                if (cont == posicion) {
                    System.out.println(cont + " " + contacto);
                }
            }
            do {
                System.out.println("Desa buascar otro contacto");
                System.out.println("1-Si");
                System.out.println("2-No");
                opcion = leer.nextInt();
            } while (opcion < 1 || opcion > 2);
        } while (opcion == 1);
    }

    public static void mostrar() {
        int cont = 0;
        for(Contacto contacto : contactos){
            cont++;
            System.out.println(cont+" "+contacto);
        }
    }

    public void actualizar() {
        int opcion;
        String nombre, numTel, numCasa, email;
        int registro, posicion, cont = 0;
        do {
            System.out.println("Introduzca el numero del registro a modificar");
            posicion = leer.nextInt();
            do {
                System.out.println("Tecla 1 en el caso de que desee modificar el nombre");
                System.out.println("Teclea 2 en el caso de que desee modificar el numero de telefono");
                System.out.println("Teclea 3 en el caso de que desee modificar el  E-mail");
                System.out.println("Teclea 4 en el caso de que desee modificar el numero de casa");
                System.out.println("Teclea el numero del dato que desees modificar: ");
                opcion = leer.nextInt();
                switch (opcion) {
                    case 1:
                        System.out.println("Intruduzca el nombre");
                        nombre = leer.nextLine();
                        nombre = leer.nextLine();
                        for (Contacto contacto : contactos) {
                            cont++;
                            if (cont == posicion) {
                                numTel = contacto.getNumTel();
                                email = contacto.getEmail();
                                numCasa = contacto.getNumCasa();
                                contacto.setNombre(nombre);
                                contacto.setNumTel(numTel);
                                contacto.setEmail(email);
                                contacto.setNumCasa(numCasa);
                            }
                        }
                        break;
                    case 2:
                        System.out.println("Intruduzca el telefono");
                        numTel = leer.nextLine();
                        numTel = leer.nextLine();
                        for (Contacto contacto : contactos) {
                            cont++;
                            if (cont == posicion) {
                                nombre = contacto.getNombre();
                                email = contacto.getEmail();
                                numCasa = contacto.getNumCasa();
                                contacto.setNombre(nombre);
                                contacto.setNumTel(numTel);
                                contacto.setEmail(email);
                                contacto.setNumCasa(numCasa);
                            }
                        }
                        break;
                    case 3:
                        System.out.println("Intruduzca el email");
                        email = leer.nextLine();
                        email = leer.nextLine();
                        for (Contacto contacto : contactos) {
                            cont++;
                            if (cont == posicion) {
                                nombre = contacto.getNombre();
                                numTel = contacto.getNumTel();
                                numCasa = contacto.getNumCasa();
                                contacto.setNombre(nombre);
                                contacto.setNumTel(numTel);
                                contacto.setEmail(email);
                                contacto.setNumCasa(numCasa);
                            }
                        }
                        break;
                    case 4:
                        System.out.println("Intruduzca el numero de la casa");
                        numCasa = leer.nextLine();
                        numCasa = leer.nextLine();
                        for (Contacto contacto : contactos) {
                            cont++;
                            if (cont == posicion) {
                                nombre = contacto.getNombre();
                                numTel = contacto.getNumTel();
                                email = contacto.getEmail();
                                contacto.setNombre(nombre);
                                contacto.setNumTel(numTel);
                                contacto.setEmail(email);
                                contacto.setNumCasa(numCasa);
                            }
                        }
                        break;
                    default:
                        System.out.println("Opcion no valida");
                }
            } while (opcion < 1 || opcion > 4);
            do {
                System.out.println("Desa buascar otro modificar");
                System.out.println("1-Si");
                System.out.println("2-No");
                opcion = leer.nextInt();
            } while (opcion < 1 || opcion > 2);
            cont=0;
        } while (opcion == 1);
    }
    
    public boolean guardar(){
        try
        {
            FileOutputStream fos = new FileOutputStream("contactoData");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(contactos);
            oos.close();
            fos.close();
            return true;
        } 
        catch (IOException ioe) 
        {
            ioe.printStackTrace();
            return false;
        }
    }
    
    public boolean guardar(Object contactos){
        try
        {
            FileOutputStream fos = new FileOutputStream("contactoData");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(contactos);
            oos.close();
            fos.close();
            return true;
        } 
        catch (IOException ioe) 
        {
            ioe.printStackTrace();
            return false;
        }
    }
    
    public boolean cargar(){
        ObjectInputStream ois;
        FileInputStream fis;
        try
        {
            fis = new FileInputStream("contactoData");
            ois = new ObjectInputStream(fis);
 
            contactos = (ArrayList) ois.readObject();
            if(contactos.size()==0){
                System.out.println("No se tienen entradas previas");
            }else{
                System.out.println("Cargando entradas previas");
            }
            ois.close();
            fis.close();
            return true;
        } 
        catch (IOException ioe) 
        {
            ioe.printStackTrace();
            return false;
        } 
        catch (ClassNotFoundException c) 
        {
            System.out.println("Class not found");
            c.printStackTrace();
            return false;
        }
    }
}
