/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import java.io.Serializable;

/**
 *
 * @author Joshua
 */
public class Contacto implements Serializable {
    private String nombre,numTel,email,numCasa;
    
 
    //Getters and setters
 
    public Contacto(String nombre, String numTel, String email, String numCasa) {
        super();
        this.nombre = nombre;
        this.numTel = numTel;
        this.numCasa = numCasa;
        this.email = email;
    }
    
    public void setNombre(String a){
        nombre = a;
    }
    
    public void setNumTel(String a){
        numTel = a;
    }
    
    public void setEmail(String a){
        email = a;
    }
    
    public void setNumCasa(String a){
        numCasa = a;
    }
    
    public String getNombre(){
        return nombre;
    }
    
    public String getNumTel(){
        return numTel;
    }
    
    public String getEmail(){
        return email;
    }
    
    public String getNumCasa(){
        return numCasa;
    }
 
    @Override
    public String toString() {
        return "Contacto [nombre=" + nombre + ", numTel=" + numTel + ", numCasa=" + numCasa + ", email=" + email + "]";
    }
}
